
var http = require('http');
var os = require("os");
var hostname = os.hostname();
fs = require('fs')

http.createServer(function(req,res){
    var path = req.url.toLowerCase();  
    switch(path) {  
        case '/':   
            serveStaticFile(res, '/index.html', 'text/html');  
            break;
        case '/ver':
            serveStaticFile(res, '/ver.html', 'text/html');  
            break;
        default:  //default case is set to serve 404 error page
            serveStaticFile(res, '/404.html', 'text/html');
            break;
}
}).listen(3000);

console.log('Node Server running on localhost:3000');

function serveStaticFile(res, path, contentType, responseCode) {
    if(!responseCode) responseCode=200;
    fs.readFile(__dirname + path, function(err, data) {
        res.writeHead(responseCode, {'Content-Type': contentType});
        res.end(data + '<br>Host: ' + hostname);
    });
}
