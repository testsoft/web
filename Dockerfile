FROM node:6.17-alpine

ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

COPY ./app $APP_HOME


EXPOSE 3000

ENTRYPOINT ["node", "app.js"]
